var mongooseConnection = 'mongodb://localhost/patata-bullida';
if(process.env.ENVIRONMENT == 'production'){
	mongooseConnection = 'mongodb://'+process.env.MONGO_DBUSER+':'+process.env.MONGO_DBPASSWORD+'@'+process.env.MONGO_URL+'/'+process.env.MONGO_DATABASE
}

module.exports = {
    url : mongooseConnection
}