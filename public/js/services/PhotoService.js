angular.module('PhotoService', []).factory('Photo', ['$http', function($http) {
	return {
		get : function(count,min_tag_id,max_tag_id) {
			if(count == undefined){
				count = 10;
			}

			var url = '/api/photos/'+count;

			if(min_tag_id != undefined){
				url += '/'+min_tag_id;

				if(max_tag_id != undefined){
					url += '/'+max_tag_id;
				} 
			}			

			return $http.get(url);
		}
	}
}]);