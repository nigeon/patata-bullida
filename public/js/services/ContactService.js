angular.module('ContactService', []).factory('Contact', ['$http', function($http) {
	return {
		send : function(data) {
			return $http.post('/api/contacts', data);
		},
		assist : function(data) {
			return $http.post('/api/guests', data);
		},
	}
}]);