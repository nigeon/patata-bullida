angular.module('WeatherService', []).factory('Weather', ['$http', function($http) {
	return {
		get : function() {
			return $http.get('/api/weathers');
		}
	}
}]);