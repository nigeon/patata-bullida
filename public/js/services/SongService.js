angular.module('SongService', []).factory('Song', ['$http', function($http) {
	return {
		get : function(page) {
			if(page == undefined) page = 0;
			return $http.get('/api/songs/'+page);
		},
		search : function(text) {
			return $http.put('/api/songs/'+text);
		},
		add : function(data) {
			return $http.post('/api/songs',data);
		},
		like : function(id) {
			return $http.put('/api/songs/like/'+id);
		}
	}
}]);