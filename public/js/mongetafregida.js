$(function() {
	// jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        var scrollT = $($anchor.attr('href')).offset().top-45;
        $('html, body').stop().animate({
            scrollTop: scrollT
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    /*$('#countdown').countdown({until:  new Date(2015, 4-1, 25, 12, 00), format: 'dHM'});*/

    $('.carousel .item').each(function(){
        var next = $(this).next();

        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length>0) {
            next.next().children(':first-child').clone().appendTo($(this));
        } else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }

    });
});