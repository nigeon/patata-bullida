angular.module('PhotoCtrl', []).controller('PhotoController', function($scope, $http, $interval, Photo) {
	$scope.photos = [];
  	$scope.minTagId = undefined;
  	$scope.maxTagId = undefined;

	getPhotos();
	$interval(getPhotos, 5000);

	function getPhotos() {
		Photo.get(25,$scope.minTagId,$scope.maxTagId)
			.success(function(data) {
				data.photos.forEach(function(img,key){
					$scope.photos.push(img);
				});
				//$scope.photos = chunk(data.photos, 3);

				if(data.next_min_id != undefined) $scope.minTagId = data.next_min_id;
				//if(data.next_max_id != undefined) $scope.maxTagId = data.next_max_id;
			});
	}

	function chunk(arr, size) {
		var newArr = [];
		for (var i=0; i<arr.length; i+=size) {
			newArr.push(arr.slice(i, i+size));
		}
		return newArr;
	}
});