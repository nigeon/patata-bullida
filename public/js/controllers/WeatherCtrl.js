angular.module('WeatherCtrl', []).controller('WeatherController', function($scope, $http, Weather) {
	$scope.weather = {};
	$scope.other = "(Ja navegarem un altre dia)";
	Weather.get(0)
		.success(function(data) {
			$scope.weather = data;
			if(data.speed*1.94384449 < 12){
				$scope.other = "(No wind, go wedding)";
			}
		});
});