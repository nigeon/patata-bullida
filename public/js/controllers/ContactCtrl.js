angular.module('ContactCtrl', []).controller('ContactController', function($scope, $http, Contact) {
	$scope.formData = {};
	$scope.contactSent = false;
	$scope.assistSent = false;

	$scope.sendContact = function() {
		Contact.send($scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.contactSent = true;
			});
	};

	$scope.sendAssist = function() {
		Contact.assist($scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.assistSent = true;
			});
	};
});