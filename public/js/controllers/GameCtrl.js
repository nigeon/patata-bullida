angular.module('GameCtrl', []).controller('GameController', function($scope, $http, Game) {
	Game.get(0)
		.success(function(data) {
			$scope.games = data.games;
			$scope.pages = data.pages;
		});

	$scope.gameId = 0;
	$scope.formScore = {};
	$scope.score = 0;
	$scope.pages = 0;
  	$scope.currentPage = 0;

	$scope.getPage = function (page) {
		Game.get(page)
			.success(function(data) {
				$scope.currentPage = page;
				$scope.pages = data.pages;
				$scope.games = data.games;
			});
	};

	$scope.startGame = function () {
		Game.start()
			.success(function(data) {
				$scope.gameId = data._id;
			});
	};

	$scope.sendScore = function (score){
		if($scope.gameId == 0){
			$route.reload();
		}

		Game.complete($scope.gameId,{"name":$scope.formScore.name,"score":$scope.score})
			.success(function(data) {
				$scope.gameId = 0;
				$scope.score = 0
				$scope.formScore = {};

				Game.get(0)
					.success(function(data) {
						$scope.games = data.games;
						$scope.pages = data.pages;
					});

				$('#scoreForm').modal('hide');
			});
	}
});