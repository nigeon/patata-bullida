angular.module('SongCtrl', []).controller('SongController', function($scope, $http, Song) {
	$scope.formData = {};
	$scope.searchItems = {};
	$scope.songs = {};
	$scope.pages = 0;
  	$scope.currentPage = 0;

	Song.get()
		.success(function(data) {
			$scope.songs = data.songs;
			$scope.pages = data.pages;
		});

	$scope.search = function() {
		$scope.searchItems = {};
		Song.search($scope.formData.text)
			.success(function(data) {
				$scope.searchItems = data.items;
			});
	};

	$scope.add = function(song) {
		var newSong = {
			'name': song.name,
			'artist': song.artists[0].name,
			'album': song.album.name,
			'img': song.album.images[2].url,
			'uri': song.uri,
			'url': song.external_urls.spotify
		};

		Song.add(newSong)
			.success(function(data) {
				Song.get()
					.success(function(data) {
						$scope.songs = data.songs;
						$scope.pages = data.pages;
					});
			});
	};

	$scope.getPage = function (page) {
		Song.get(page)
			.success(function(data) {
				$scope.currentPage = page;
				$scope.pages = data.pages;
				$scope.songs = data.songs;
			});
	};

	$scope.like = function(song){
		song.likeHide = true;
		Song.like(song._id).success(function(data) {
			Song.get()
				.success(function(data) {
					$scope.songs = data.songs;
					$scope.pages = data.pages;
				});
		});
	}
});