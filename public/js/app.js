angular.module('patataBullidaApp', ['ngRoute', 'appRoutes', 'angular-loading-bar', 'ngAnimate', 'MainCtrl', 'ContactCtrl', 'ContactService','GameCtrl', 'GameService','PhotoCtrl', 'PhotoService'])
	.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
		cfpLoadingBarProvider.includeSpinner = false;
	}]);