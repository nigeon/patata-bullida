exports.findAll = function(req, res){
	var hashTag = 'patatabullida2504';

	var options = {};
	if(typeof req.params.min_id !== 'undefined') options.min_tag_id = req.params.min_id;
	if(typeof req.params.max_id !== 'undefined') options.max_tag_id = req.params.max_id;
	if(typeof req.params.count !== 'undefined') options.count = req.params.count;
	
	var api = require('instagram-node').instagram();
	api.use({ client_id: 'c10110a7879c49138fe4f892b73d1e98',
	         client_secret: '87121ba4b86d42949ca358d0d36bf027' });

	api.tag_media_recent(hashTag, options,function(err, result, pagination, remaining, limit) {
		if(err){
			console.error(err.stack);
			res.send(err);
		}else{
			var photos = [];
			for(var img in result){
				var imgId = result[img].id.split("_");
				photos.push({"id":imgId[0], "link": result[img].link, "user": result[img].user.full_name, "text":result[img].caption.text, "images":result[img].images});
			}
			res.json({"photos":photos,"next_max_id":pagination.next_max_id,"next_min_id":pagination.next_min_id});
		}
	});

};