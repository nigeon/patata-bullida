var Song = require(__baseApp + 'models/song');

exports.getSpotifyPlaylistSongs = function(req, res){
	var SpotifyWebApi = require('spotify-web-api-node');
	var spotifyApi = new SpotifyWebApi({
		clientId : process.env.SPOTIFY_CLIENTID,
		clientSecret : process.env.SPOTIFY_CLIENTSECRET
	});

	// Retrieve an access token.
	spotifyApi.clientCredentialsGrant()
	  .then(function(data) {
	    console.log('The access token expires in ' + data['expires_in']);
	    console.log('The access token is ' + data['access_token']);

	    // Save the access token so that it's used in future calls
	    spotifyApi.setAccessToken(data['access_token']);

		//Get playlist tracks    
	    spotifyApi.getPlaylistTracks(process.env.SPOTIFY_USER, process.env.SPOTIFY_PLAYLIST)
		  .then(function(data) {
		    res.send(data.items);
		  }, function(err) {
		    console.log('Something went wrong!', err);
		  });

	  }, function(err) {
	        console.log('Something went wrong when retrieving an access token', err);
	  });

	console.log('done');
};

exports.search = function(req, res){
	var text = req.params.text;
	var SpotifyWebApi = require('spotify-web-api-node');
	var spotifyApi = new SpotifyWebApi({
		clientId : process.env.SPOTIFY_CLIENTID,
		clientSecret : process.env.SPOTIFY_CLIENTSECRET
	});

	spotifyApi.searchTracks(text)
		.then(function(data) {
			res.send(data.tracks);

			/*
		    // Print some information about the results
		    console.log('I got ' + data.tracks.total + ' results!');

		    // Go through the first page of results
		    var firstPage = data.tracks.items;
		    console.log('The tracks in the first page are.. (popularity in parentheses)');

		    firstPage.forEach(function(track, index) {
		      console.log(index + ': ' + track.name + ' (' + track.popularity + ')');
		    });
		    */

		}, function(err) {
		    console.error(err);
		});
};

exports.addToPlaylist = function(req,res){
	var song = new Song();
	song.uri = req.body.uri;
	song.name = req.body.name;
	song.artist = req.body.artist,
	song.img = req.body.img;
	song.url = req.body.url;
	song.album = req.body.album;
	song.likes = 0;

	try{
		song.save(function(err) {
			if(err){
				console.error(err.stack);
				res.send(err);
			}else{
				res.json(song);	
			}
		});
	}catch (e) {
		console.log("Error log");
		console.log(e);
	}
	
};

exports.findAll = function(req, res){
	Song.find().sort({"likes":-1,"created":-1}).limit(10).skip(10*req.params.page).exec(function(err, songs) {
	    if (err){
	    	console.error(err.stack);
	        res.send(err);
	    }else{
	    	Song.count().exec(function(err,count){
	    		var pages = Math.ceil(count/10);
				res.json({"pages":pages, "songs":songs});
	    	});
	    }
	});
};

exports.like = function(req, res){
	var remoteIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	Song.findById(req.params.id).ne('likedIps',remoteIp).exec(function(err,song){
		if(!song){
			res.json({"success":0});	
		}else{
			if (err){
		    	console.error(err);
		        res.send(err);
		    }else{
		    	song.likes++;
		    	song.likedIps.push(remoteIp);
		    	try{
					song.save(function(err) {
						if(err){
							console.error(err.stack);
							res.send(err);
						}else{
							res.json({"song":song,"success":1});	
						}
					});
				}catch (e) {
					console.log("Error log");
					console.log(e);
				}
		    }
		}
	});
};