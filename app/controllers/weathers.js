var Weather = require(__baseApp + 'models/weather');

exports.getForecast = function(req, res){
	var cnt = 16;
	var http = require('http');

	var options = {
		hostname: 'api.openweathermap.org',
		path: '/data/2.5/forecast/daily?q=Barcelona,Spain&cnt='+cnt+'&lang=ca&units=metric&APPID=af2e195deec09d473bf368dc2be071fa',
		port: 80,
		method: 'GET'
	};

	var reqForecast = http.get(options, function(resForecast) {
		var bodyChunks = [];
		resForecast.on('data', function(chunk) {
			bodyChunks.push(chunk);
		}).on('end', function() {
			var body = JSON.parse(Buffer.concat(bodyChunks));

			if(body.cod == 200){ //So, we got this from the API, we'll save & send it
				var weddingForecast = {};
				body.list.forEach(function(day,key){
					if(day.dt == 1429959600){
						weddingForecast = day;
					}
				});

				weddingForecast.windName = getWindName(weddingForecast.deg);

				var weather = new Weather(weddingForecast);
				weather.save(function(err) {
					res.json(weddingForecast);
				});
			}else{ //API didn't work... we'll get our last forecast!
				Weather.find().sort({"created":-1}).limit(1).exec(function(err, weather) {
					if (err){
						console.error(err.stack);
						res.send(err);
					}else{
						res.json(weather);
					}
				});
			}
		});
	});

	reqForecast.on('error', function(e) {
	  console.log('ERROR: ' + e.message);
	});
};

function getWindName(deg){
	if(deg > 337.5 && deg < 22.5){
		return "Tramuntana";
	}else if(deg > 22.5 && deg < 67.5){
		return "Gregal";
	}else if(deg > 67.5 && deg < 112.5){
		return "Llevant";
	}else if(deg > 112.5 && deg < 157.5){
		return "Xaloc";
	}else if(deg > 157.5 && deg < 202.5){
		return "Migjorn";
	}else if(deg > 202.5 && deg < 247.5){
		return "Garbí";
	}else if(deg > 247.5 && deg < 292.5){
		return "Ponent";
	}else if(deg > 292.5 && deg < 337.5){
		return "Mestral";
	}
};