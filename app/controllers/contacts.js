var Contact = require(__baseApp + 'models/contact');

exports.send = function(req, res){
	var contact = new Contact();
	contact.name = req.body.name; 
	contact.email = req.body.email;
	contact.text = req.body.text;
	
	var sendgrid  = require('sendgrid')(process.env.SENDGRID_USERNAME, process.env.SENDGRID_PASSWORD);
	var payload   = {
	  to      : 'patatabullida2504@gmail.com',
	  from    : contact.email,
	  subject : 'Forumari de contacte: ' + contact.name + '('+contact.email+')',
	  text    : contact.text
	}
	sendgrid.send(payload, function(err, json) {
		if (err) { console.error(err); }
		console.log(json);
	});

	contact.save(function(err) {
		if(err){
			console.error(err.stack);
			res.send(err);
		}else{
			res.json(contact);	
		}
	});
};