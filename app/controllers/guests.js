var Guest = require(__baseApp + 'models/guest');

exports.send = function(req, res){
	var guest = new Guest();

	for (var key in req.body) {
		guest[key] = req.body[key];
	}

	var sendgrid  = require('sendgrid')(process.env.SENDGRID_USERNAME, process.env.SENDGRID_PASSWORD);
	var payload   = {
	  to      : 'patatabullida2504@gmail.com',
	  from    : guest.email,
	  subject : "Forumari de d'assistència: " + guest.name + '('+guest.email+')',
	  text    : JSON.stringify(guest)
	}

	sendgrid.send(payload, function(err, json) {
		if (err) { console.error(err); }
		console.log(json);
	});

	guest.save(function(err) {
		if(err){
			console.error(err.stack);
			res.send(err);
		}else{
			res.json(guest);
		}
	});
};