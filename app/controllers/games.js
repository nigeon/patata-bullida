var Game = require(__baseApp + 'models/game');

//Get the leaderboard
exports.findAll = function(req, res){
	var requestedPage = req.params.page;
	if(requestedPage == "undefined") requestedPage = 0;

	Game.aggregate(
		{ $match: {completed: 1 }},
		{ $sort: { score: -1 }},
		{ $group: { _id: '$name', 'doc' : { "$first" : "$$ROOT"}}},
		{ $project: { _id: "$doc._id", name: "$doc.name", score: "$doc.score", updated: "$doc.updated" }},
		{ $sort: { score: -1, updated: 1 }},
		{ $skip: (10*requestedPage)},
		{ $limit: 10 },
		function(err,result){
			if (err){
	    		console.error(err.stack);
	        	res.send(err);
			}else{
				Game.find({completed:1}).distinct('name').exec(function(err,count){
	    			if (err){
			    		console.error(err.stack);
			        	res.send(err);
					}else{
						pages = Math.ceil(count.length/10);
	    				res.json({"pages":pages, "games":result});
	    			}
	    		});
			}
		}
	);
};

//Create a new game, and return a token
exports.create = function(req, res){
	var game = new Game();
	game.completed = 0;
	game.score = 0;
	game.name = '';

	game.save(function(err) {
		if(err){
			console.error(err.stack);
			res.send(err);
		}else{
			res.json(game);	
		}
	});
};

//Save the game as finished
exports.save = function(req, res){
	var moment = require('moment');

	Game.findById(req.params.id, function(err, game) {
		if (err){
			console.error(err.stack);
			res.send(err);
		}else{
			try {
				//Decrypt AES.
				var CryptoJS = require("crypto-js");
				var decrypted = CryptoJS.AES.decrypt(req.body.data, 'gameId'+req.params.id+'ba23co-n');
				data = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));

				//Preparing the object and saving
				game.name = data.name;
				game.score = data.score;
				game.completed = 1;
				game.updated = moment().format();

				game.save(function(err) {
					if(err){
						console.error(err.stack);
						res.send(err);
					}else{
						res.json(game);	
					}
				});
			}catch (e) {
			  console.log("Errror log");
			  console.log(e);
			}
		}
	});
};