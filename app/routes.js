module.exports = function(app) {
    // server routes ===========================================================

    //Middleware for all calls
    app.use(function(req, res, next) {
        console.log('Request: ' + req.method + ' -> ' + req.originalUrl);
        next();
    });

    /**
     * Tetris Games
     */

    //Get score board
    app.get(['/api/games','/api/games/:page'], function(req, res) {
        var GamesController = require(__baseApp + 'controllers/games');
        GamesController.findAll(req,res);
    });

    //Get new game ID Start new game
    app.post('/api/games', function(req, res) {
        var GamesController = require(__baseApp + 'controllers/games');
        GamesController.create(req,res);
    });

    //Save game score
    app.put('/api/games/:id', function(req, res) {
        var GamesController = require(__baseApp + 'controllers/games');
        GamesController.save(req,res);
    });

    //Songs
    app.get(['/api/songs','/api/songs/:page'], function(req, res) {
        var SongsController = require(__baseApp + 'controllers/songs');
        SongsController.findAll(req,res);
    });

    app.put('/api/songs/:text',function(req,res){
        var SongsController = require(__baseApp + 'controllers/songs');
        SongsController.search(req,res);
    });

    app.post('/api/songs',function(req,res){
        var SongsController = require(__baseApp + 'controllers/songs');
        SongsController.addToPlaylist(req,res);
    });

    app.put('/api/songs/like/:id',function(req,res){
        var SongsController = require(__baseApp + 'controllers/songs');
        SongsController.like(req,res);
    });

    //Contact
    app.post('/api/contacts',function(req,res){
        var ContactsController = require(__baseApp + 'controllers/contacts');
        ContactsController.send(req,res);
    });

    app.post('/api/guests',function(req,res){
        var GuestsController = require(__baseApp + 'controllers/guests');
        GuestsController.send(req,res);
    });

    //Photos
    app.get(['/api/photos','/api/photos/:count/:min_id?/:max_id?'], function(req, res) {
        var PhotosController = require(__baseApp + 'controllers/photos');
        PhotosController.findAll(req,res);
    });

    //Weather
    app.get(['/api/weathers','/api/weather'], function(req, res) {
        var WeathersController = require(__baseApp + 'controllers/weathers');
        WeathersController.getForecast(req,res);
    });

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', function(req, res) {
        res.sendFile(__base + 'public/views/index.html'); // load our public/index.html file
    });
};