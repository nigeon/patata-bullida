var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContactSchema   = new Schema({
        name: String,
        email: {type: [String], index: true},
        text: String,
        created: { type: Date, default: Date.now }
    },
    { autoIndex: true }
);

module.exports = mongoose.model('Contact', ContactSchema);