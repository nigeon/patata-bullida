var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GameSchema   = new Schema({
		completed: Number,
		score: Number,
        name: String,
        created: { type: Date, default: Date.now },
        updated: { type: Date, default: Date.now }
    },
    { autoIndex: true }
);

module.exports = mongoose.model('Game', GameSchema);