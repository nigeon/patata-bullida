var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GuestSchema   = new Schema({
        name: String,
        email: {type: [String], index: true},
        text: String,
        dinar: {type: Boolean, default: 0},
        festa: {type: Boolean, default: 0},
        beer: {type: Boolean, default: 0},
        vi: {type: Boolean, default: 0},
        ron: {type: Boolean, default: 0},
        whisky: {type: Boolean, default: 0},
        gin: {type: Boolean, default: 0},
        vodka: {type: Boolean, default: 0},
        dormir: {type: Boolean, default: 0},
        created: { type: Date, default: Date.now }
    },
    { autoIndex: true }
);

module.exports = mongoose.model('Guest', GuestSchema);