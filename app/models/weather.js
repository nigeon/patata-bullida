var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WeatherSchema   = new Schema({
        "temp": {
            "day": Number,
            "min": Number,
            "max": Number,
            "night": Number,
            "eve": Number,
            "morn": Number
        },
        "pressure": Number,
        "humidity": Number,
        "weather": {
            "id": Number,
            "main": String,
            "description": String,
            "icon": String
        },
        "speed": Number,
        "deg": Number,
        "windName": String,
        "clouds": Number,
        "rain": Number,
        "created": { type: Date, default: Date.now }
    },
    { autoIndex: true }
);

module.exports = mongoose.model('Weather', WeatherSchema);