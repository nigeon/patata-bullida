var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SongSchema   = new Schema({
        "uri": String,
        "name": String,
        "artist": String,
        "album": String,
        "img": String,
        "url": String,
        "likes": Number,
        "likedIps": Array,
        created: { type: Date, default: Date.now }
    },
    { autoIndex: true }
);

module.exports = mongoose.model('Song', SongSchema);